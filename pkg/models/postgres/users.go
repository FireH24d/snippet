package postgres

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/di.mar/golang/pkg/models"
	"golang.org/x/crypto/bcrypt"
	"strings"
	"time"
)

const (
	insertUser   = "INSERT INTO users (name,email,hashed_password,created) VALUES ($1,$2,$3,$4) RETURNING id"
	authenticate = "SELECT id, hashed_password FROM users WHERE email = $1 AND active = TRUE"
	getUser      = "SELECT id, name, email, created, active FROM users WHERE id = $1"
)

type UserModel struct {
	Pool *pgxpool.Pool
}

// We'll use the Insert method to add a new record to the users table.
func (m *UserModel) Insert(name, email, password string) error {
	var id uint64
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}
	row := m.Pool.QueryRow(context.Background(), insertUser, name, email, hashedPassword, time.Now())
	err = row.Scan(&id)
	if err != nil {
		if strings.Contains(err.Error(), "pq: duplicate key value violates unique constraint \"users_uc_email\"") {
			return models.ErrDuplicateEmail
		}

		return err
	}
	return nil
}

// We'll use the Authenticate method to verify whether a user exists with
// the provided email address and password. This will return the relevant
// user ID if they do.
//

func (m *UserModel) Authenticate(email, password string) (int, error) {
	var id int
	var hashedPassword []byte
	row := m.Pool.QueryRow(context.Background(), authenticate, email)
	err := row.Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, models.ErrNoRecord
		}
	}
	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	return id, nil
}

func (m *UserModel) Get(id int) (*models.User, error) {
	u := &models.User{}
	err := m.Pool.QueryRow(context.Background(), getUser, id).Scan(&u.ID, &u.Name, &u.Email, &u.Created, &u.Active)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return u, nil
}
